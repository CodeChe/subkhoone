// import components
import React from 'react';
import {Button} from "antd";

// import style
import './style/generic.scss';
import './App.scss';
import 'antd/dist/antd.css';

// import assets
import logo from './assets/svg/logo.svg';
import userIcon from './assets/svg/user.svg';
import searchIcon from './assets/svg/search.svg';

function App() {
    return (
        <div className="frame-of-content">
            <div className="navbar">
                <div className="logo">
                    <img src={logo} alt="logo of subkhoone company"/>
                </div>
                <div className="menu">
                    <p className="ml-2">خانه</p>
                    <p className="ml-2">املاک</p>
                    <p className="ml-2">چرا ما</p>
                    <p className="ml-2">درباره ما</p>
                    <p className="ml-2">تماس باما</p>
                </div>
                <div className="left-of-nav">
                    <img className="mr-2" src={searchIcon} alt="search icon"/>
                    <img className="mr-2" src={userIcon} alt="user icon"/>
                    <Button className="primaryBtn mr-2" type="primary" >ملک خود را ثبت کنید</Button>
                </div>
            </div>
        </div>
    );
}

export default App;
